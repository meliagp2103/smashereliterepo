﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

namespace CustomFramework.Managers
{
    public abstract class AAdvertiseSystem : IUnityAdsListener
    {
        protected AdvertiseSystemSetting _settings;
        private string AndroidID => _settings.GameIdAndroid;
        private string IOSId => _settings.GameIdIOS;
        private bool IsTestMode => _settings.IsTestMode;
        private List<AdsPairing> AdsPairingsList => _settings.AdsPaitingsList;
        private BannerPairing BannerInfo => _settings.BannerInfo;

        private bool _isBannerShowing;

        protected virtual void Initialize()
        {
            #if UNITY_IOS
            Advertisement.Initialize(IOSId, IsTestMode);
            #endif
            #if UNITY_ANDROID || UNITY_EDITOR
            Advertisement.Initialize(AndroidID, IsTestMode);
            #endif
            Advertisement.AddListener(this);
        }

        public virtual void ShowAd(AdsType type)
        {
            try
            {
                var adsName = AdsPairingsList
                    .Single(pair => pair.AdsType == type)
                    .AdsDashboardName;

                if (Advertisement.isInitialized && Advertisement.IsReady(adsName))
                {
                    Advertisement.Show(adsName);
                }

            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public async void ShowBanner()
        {
            if (!BannerInfo.ShowBanner || _isBannerShowing)
                return;

            _isBannerShowing = true;

            while (!Advertisement.isInitialized)
                await Task.Yield();

            while(!Advertisement.IsReady(BannerInfo.BannerName))
                await Task.Yield();

            Advertisement.Banner.SetPosition(BannerInfo.BannerPosition);
            Advertisement.Banner.Show(BannerInfo.BannerName);
        }

        public virtual void OnUnityAdsReady(string placementId)
        {
            throw new System.NotImplementedException();
        }

        public virtual void OnUnityAdsDidError(string message)
        {
            throw new System.NotImplementedException();
        }

        public virtual void OnUnityAdsDidStart(string placementId)
        {
            throw new System.NotImplementedException();
        }

        public virtual void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            throw new System.NotImplementedException();
        }
    }

} 
