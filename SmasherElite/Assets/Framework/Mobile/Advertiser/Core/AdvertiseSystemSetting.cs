﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.Managers
{
    [CreateAssetMenu(menuName = "Mobile/Settings/advertise")]
    public class AdvertiseSystemSetting : ScriptableObject
    {
        public string GameIdIOS;
        public string GameIdAndroid;

        public BannerPairing BannerInfo;

        public bool IsTestMode = true;

        public List<AdsPairing> AdsPaitingsList;
    }
}
