﻿namespace CustomFramework.Managers
{
    [System.Serializable]
    public struct AdsPairing
    {
        public string AdsDashboardName;
        public AdsType AdsType;
    }
}
