﻿using UnityEngine.Advertisements;

namespace CustomFramework.Managers
{
    [System.Serializable]
    public struct BannerPairing
    {
        public string BannerName;
        public BannerPosition BannerPosition;
        public bool ShowBanner;
    }

}
