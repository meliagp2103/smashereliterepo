﻿using System;
using UnityEngine;

namespace CustomFramework.UI
{
    public class BasePanel : MonoBehaviour, IDisposable
    {
        public void Active() => gameObject.SetActive(true);
        public void Dispose() => gameObject.SetActive(false);
    }
}
