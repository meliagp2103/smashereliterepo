﻿using CustomFramework.ServiceLocator.Exceptions;
using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.ServiceLocator
{
    public static class ServiceLocatorSystem
    {
        private static bool isProjectLocatorTryLoad = false;
        public static ALocator SceneLocator { get; private set; }
        public static ALocator ProjectLocator { get; private set; }

        public static void UpdateLocator(ALocator sceneLocator)
        {
            SceneLocator = sceneLocator;
            if(!isProjectLocatorTryLoad &&!ProjectLocator)
            {
                isProjectLocatorTryLoad = true;
                var projectLocatorPrefab = Resources.Load<ALocator>("Project Locator");
                ProjectLocator = sceneLocator.instantiateObject(projectLocatorPrefab);
            }
        }

        public static Registration Register<T>(T user)
        {
            var registration = new Registration<T>(user);
            return registration;
        }

        public static Research<T> GetOrCreateSystem<T>() where T : new()
        {
            return new Research<T>();
        }

        public static List<(T item, string id)> ResolveDependecy<T>()
        {
            var itemList = new List<(T, string)>();

            if(ProjectLocator)
                itemList.AddRange(ProjectLocator?.GetItemList<T>());

            if(SceneLocator)
                itemList.AddRange(SceneLocator.GetItemList<T>());

            if (itemList.Count == 0)
                throw new NotRegisteredItemException($"SERVICE LOCATOR: item of type {typeof(T)} not found");

            return itemList;
        }

    }
}
