﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CustomFramework.ServiceLocator
{
    public abstract class ALocator : MonoBehaviour
    {
#pragma warning disable 649
        [SerializeField] private List<ARegister> RegisterList;
#pragma warning restore 649

        protected List<Registration> _registeredItems = new List<Registration>();
        protected virtual void Awake()
        {
            
        }


        protected virtual void Start()
        {
            RegisterList.ForEach(reg => reg.Register());
        }

        public void AddRegistration(Registration registration)
        {
            _registeredItems.Add(registration);
        }
        public List<(T item, string id)> GetItemList<T>()
        {
            var itemList = new List<(T, string)>();
            _registeredItems
                .ForEach(item => {
                    if (item.Item is T tmp_item)
                        itemList.Add((tmp_item, item.ID));
                });

            return itemList;
        }

        public T instantiateObject<T>(T obj) where T : Component
        {
            return Instantiate(obj);
        }
    }
}

