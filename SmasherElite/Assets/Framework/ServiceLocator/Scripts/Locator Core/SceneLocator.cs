﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CustomFramework.ServiceLocator
{ 
    public class SceneLocator : ALocator
    {
        protected override void Awake()
        {
            ServiceLocatorSystem.UpdateLocator(this);
            base.Awake();
        }
    }
}
