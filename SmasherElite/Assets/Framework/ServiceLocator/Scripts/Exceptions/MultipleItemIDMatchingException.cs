﻿using System;
namespace CustomFramework.ServiceLocator.Exceptions
{
    public class MultipleItemIDMatchingException : Exception
    {
        public MultipleItemIDMatchingException(string message) : base(message)
        {
        }
    }
}
