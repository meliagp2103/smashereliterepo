﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.ServiceLocator
{
    public abstract class ARegister : MonoBehaviour
    {
        public abstract void Register();
    }
}

