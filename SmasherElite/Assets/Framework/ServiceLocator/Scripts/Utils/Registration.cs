﻿namespace CustomFramework.ServiceLocator
{
    public class Registration
    {
        public string ID;
        public object Item { get; protected set; }
    }

    public class Registration<T> : Registration
    {
        public Registration(T item)
        {
            Item = item;
        }
    }
}


