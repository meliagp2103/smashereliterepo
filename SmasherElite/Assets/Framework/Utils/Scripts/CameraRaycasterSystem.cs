﻿using UnityEngine;

namespace CustomFramework.Utils
{
    public class CameraRaycasterSystem
    {
        private Camera _camera;

        public CameraRaycasterSystem()
        {
            _camera = Camera.main;
        }

        public RaycastHit GetRaycastFromCameraToWorld(Vector2 screenPosition)
        {
            RaycastHit hit;
            var cameraRay = _camera.ScreenPointToRay(screenPosition);

            Physics.Raycast(cameraRay, out hit);
            return hit;
        }
    }
}

