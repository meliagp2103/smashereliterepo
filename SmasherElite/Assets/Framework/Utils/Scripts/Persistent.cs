﻿using UnityEngine;
namespace CustomFramework.Utils
{
    public class Persistent : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}

