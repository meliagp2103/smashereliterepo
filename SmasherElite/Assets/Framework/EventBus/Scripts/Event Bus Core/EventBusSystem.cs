﻿using CustomFramework.EventBus.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomFramework.EventBus
{
    public class EventBusSystem
    {
        private List<Signal> _actionList = new List<Signal>();
        public void InvokeEvent<T>(T param)
        {
            var tmp_event = _actionList
                .Where(obj => obj.GetType() == typeof(Signal<T>))
                .SingleOrDefault() as Signal<T>;

            if (tmp_event == null)
                throw new NotDeclaredSignalException($"Signal Not Declared {typeof(T)}");

            tmp_event.Action?.Invoke(param);
        }

        public void DeclareEvent<T>()
        {
            var tmp_event = _actionList
                .Where(obj => obj.GetType() == typeof(Signal<T>))
                .SingleOrDefault() as Signal<T>;

            if (tmp_event != null)
                throw new AlreadyDecalredSignalException($"Signal Already Declared {typeof(T)}");

            _actionList.Add(new Signal<T>());
        }
        public void Subscribe<T>(Action<T> subscriber)
        {
            var tmp_event = _actionList
                .Where(obj => obj.GetType() == typeof(Signal<T>))
                .SingleOrDefault() as Signal<T>;

            if (tmp_event == null)
                throw new NotDeclaredSignalException($"Signal Not Declared {typeof(T)}");

            tmp_event.Action += subscriber;
        }

        public void Unsubscribe<T>(Action<T> unsubscriber)
        {
            var tmp_event = _actionList
                .Where(obj => obj.GetType() == typeof(Signal<T>))
                .SingleOrDefault() as Signal<T>;

            if (tmp_event == null) throw new NotDeclaredSignalException($"Signal Not Declared {typeof(T)}");

            tmp_event.Action -= unsubscriber;
        }
    }
}

