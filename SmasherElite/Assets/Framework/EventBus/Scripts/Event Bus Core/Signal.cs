﻿namespace CustomFramework.EventBus
{
    using System;

    public class Signal { }
    public class Signal<T> : Signal
    {
        public Action<T> Action;
    }
}

