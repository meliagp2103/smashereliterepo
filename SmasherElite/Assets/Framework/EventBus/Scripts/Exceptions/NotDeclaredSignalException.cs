﻿namespace CustomFramework.EventBus.Exceptions
{
    using System;

    public class NotDeclaredSignalException : Exception
    {
        public NotDeclaredSignalException(string message) : base(message)
        {
        }
    }
}

