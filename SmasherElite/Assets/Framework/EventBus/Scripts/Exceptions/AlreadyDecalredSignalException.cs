﻿
namespace CustomFramework.EventBus.Exceptions
{
    using System;

    public class AlreadyDecalredSignalException : Exception
    {
        public AlreadyDecalredSignalException(string message) : base(message)
        {
        }
    }
}
