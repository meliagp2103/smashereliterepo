﻿namespace CustomFramework.Managers
{
    public struct GestureInfo
    {
        public Gesture Gesture;
        public GesturePhase GesturePhase;
    }
}

