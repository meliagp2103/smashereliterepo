﻿using CustomFramework.Utils;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CustomFramework.Managers
{
    public abstract class AGenericMemoryPoolBase<T> : MonoBehaviour, IMemoryPool
    where T : MonoBehaviour, IPoolable
    {
#pragma warning disable 649
        [SerializeField] private int _initalSize;
        [SerializeField] private bool _canExpand;
        [SerializeField] protected T Item;
#pragma warning restore 649
        private List<T> _objectsList = new List<T>();
        protected T FirsItem
        {
            get
            {
                var currentItem = _objectsList.FirstOrDefault(item => !item.gameObject.activeSelf);

                if (!currentItem && _canExpand)
                    currentItem = AddObbject();
                return currentItem;
            }
        }

        protected virtual void Awake()
        {
            Init();
        }

        private void Init()
        {
            for (int i = 0; i < _initalSize; i++)
                AddObbject();
        }

        private T AddObbject()
        {
            var currentItem = Instantiate(Item, this.transform);
            currentItem.OnCreate();
            _objectsList.Add(currentItem);
            currentItem.gameObject.SetActive(false);
            return currentItem;
        }

        public void Despawn(object obj)
        {
            Despawn(obj as T);
        }

        public void Despawn(T item)
        {
            item.gameObject.SetActive(false);
            item.OnDespawn();
        }
    }


    public abstract class AMemoryPool<T> : AGenericMemoryPoolBase<T>, IMemoryPool<T>
    where T : MonoBehaviour, IPoolable<IMemoryPool>
    {
        public T Spawn()
        {
            var currentItem = FirsItem;
            currentItem?.gameObject.SetActive(true);
            currentItem?.OnSpawn(this);
            return currentItem;
        }
    }

    public abstract class AMemoryPool<T1, T2> : AGenericMemoryPoolBase<T2>, IMemoryPool<T1, T2>
        where T2 : MonoBehaviour, IPoolable<T1, IMemoryPool>
    {
        public T2 Spawn(T1 param1)
        {
            var currentItem = FirsItem;
            currentItem?.gameObject.SetActive(true);
            currentItem?.OnSpawn(param1, this);
            return currentItem;
        }
    }

    public abstract class AMemoryPool<T1, T2, T3> : AGenericMemoryPoolBase<T3>, IMemoryPool<T1, T2, T3>
        where T3 : MonoBehaviour, IPoolable<T1, T2, IMemoryPool>
    {
        public T3 Spawn(T1 param1, T2 param2)
        {
            var currentItem = FirsItem;
            currentItem?.gameObject.SetActive(true);
            currentItem?.OnSpawn(param1, param2, this);
            return currentItem;
        }
    }
}

