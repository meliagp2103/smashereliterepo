﻿
namespace CustomFramework.Managers
{
    public interface IPoolable
    {
        void OnCreate();
        void OnDespawn();
    }

    public interface IPoolable<T> : IPoolable where T : IMemoryPool
    {
        void OnSpawn(T param1);
    }

    public interface IPoolable<T1, T2> : IPoolable where T2 : IMemoryPool
    {
        void OnSpawn(T1 param1, T2 param2);
    }

    public interface IPoolable<T1, T2, T3> : IPoolable where T3 : IMemoryPool
    {
        void OnSpawn(T1 param1, T2 param2, T3 param3);
    }
}
