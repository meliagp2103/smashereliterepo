﻿public enum GameState
{
    Running,
    Pause,
    EndGame,
}
