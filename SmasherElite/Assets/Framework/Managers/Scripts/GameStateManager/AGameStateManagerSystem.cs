﻿namespace CustomFramework.Managers
{
    public abstract class AGameStateManagerSystem
    {
        private GameState _gameState;
        public GameState GameState
        {
            get => _gameState;
            protected set
            {
                _gameState = value;
                OnGameStateChange(value);
            }
        }
        protected virtual void OnGameStateChange(GameState newState) { }
        public virtual void SetRunState() => GameState = GameState.Running;
        public virtual void TogglePauseState()
        {
            switch(GameState)
            {
                case GameState.Running:
                    GameState = GameState.Pause;
                    break;

                case GameState.Pause:
                    GameState = GameState.Running;
                    break;
            }
        }
        public virtual void SetEndGameState() => GameState = GameState.EndGame;
    }
}
