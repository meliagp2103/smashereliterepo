﻿namespace CustomFramework.Managers
{
    public interface ISavable
    {
        void Save();
        void Load();
        void ResetData();
    }
}

