﻿namespace CustomFramework.Managers.Exceptions
{
    using System;
    public class AudioPairingIDMatchException : Exception
    {
        public AudioPairingIDMatchException(string message) : base(message)
        {
        }
    }
}

