﻿using CustomFramework.Managers.Exceptions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CustomFramework.Managers
{
    public abstract class AAudioManagerSystem
    {
        protected AudioClipContainer audioSettings;
        protected AudioSource audioSource;

        public List<AudioClipPairing> AudioClipPairingList => audioSettings.AudioClipPairingList;
        public AAudioManagerSystem() => Init();

        protected abstract void Init();

        public AudioClip GetClipByID(string id)
        {
            try
            {
                return AudioClipPairingList
                    .Single(pairing => pairing.Identifier == id)
                    .Clip;
            }
            catch( System.Exception )
            {
                Debug.Log(AudioClipPairingList.Count);
                throw new AudioPairingIDMatchException($"AUDIO MANAGER: sfx pairing \"{id}\" not found");
            }
        }
        public void PlayOneShotSound(string id)
        {
            try
            {
                audioSource.PlayOneShot(GetClipByID(id));
            }
            catch(System.NullReferenceException)
            {
                throw new System.NullReferenceException("AUDIO MANAGER SYSTEM: audiosource dependecy not resolved");
            }
        }

        public void PlayLoopSound(string id)
        {
            try
            {
                audioSource.clip = GetClipByID(id);
                audioSource.Play();
            }
            catch (System.NullReferenceException)
            {
                throw new System.NullReferenceException("AUDIO MANAGER SYSTEM: audiosource dependecy not resolved");
            }
        }

        public void StopSound()
        {
            try
            {
                audioSource.Stop();
            }
            catch(System.NullReferenceException)
            {
                throw new System.NullReferenceException("AUDIO MANAGER SYSTEM: audiosource dependecy not resolved");
            }
        }
    }
}

