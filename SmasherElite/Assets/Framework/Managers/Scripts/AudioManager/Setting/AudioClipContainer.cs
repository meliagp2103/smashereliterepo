﻿using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.Managers
{
    [CreateAssetMenu(menuName = "Custom/Settings/AudioClipContainer", fileName = "Audio Clip Container")]
    public class AudioClipContainer : ScriptableObject
    {
        public List<AudioClipPairing> AudioClipPairingList;
    }
}

