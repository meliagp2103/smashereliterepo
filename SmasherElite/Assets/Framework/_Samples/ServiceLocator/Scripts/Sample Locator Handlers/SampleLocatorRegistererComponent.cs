﻿using UnityEngine;

namespace CustomFramework.ServiceLocator.Sample
{
    public class SampleLocatorRegistererComponent : MonoBehaviour
    {
        public void SampleDebug()
        {
            Debug.Log("Hi i'm sample script registered to locator");
        }
    }
}

