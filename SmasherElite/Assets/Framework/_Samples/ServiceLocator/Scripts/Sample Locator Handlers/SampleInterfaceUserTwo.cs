﻿using UnityEngine;

namespace CustomFramework.ServiceLocator.Sample
{
    public class SampleInterfaceUserTwo : MonoBehaviour, ISampleInterface
    {
        public void Sample()
        {
            Debug.Log("Sample 2 interface user");
        }
    }
}

