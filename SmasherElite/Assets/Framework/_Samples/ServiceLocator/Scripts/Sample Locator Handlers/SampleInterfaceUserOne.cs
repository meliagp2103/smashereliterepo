﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.ServiceLocator.Sample
{
    public class SampleInterfaceUserOne : MonoBehaviour, ISampleInterface
    {
        public void Sample()
        {
            Debug.Log("Sample 1 interface user");
        }
    }
}

