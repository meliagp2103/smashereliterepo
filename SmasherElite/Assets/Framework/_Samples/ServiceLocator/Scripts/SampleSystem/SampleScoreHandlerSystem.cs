﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.ServiceLocator.Sample
{
    public class SampleScoreHandlerSystem
    {
        public int Score { get; private set; }
        public void ScoreUp() => Score++;

    }

}
