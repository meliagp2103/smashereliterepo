﻿using UnityEngine.SceneManagement;

namespace CustomFramework.ServiceLocator.Sample
{
    public class SampleSceneHandlerSystem
    {
        public void LoadMainScene() => SceneManager.LoadScene(0);
        public void LoadSecondScene() => SceneManager.LoadScene(1);
    }
}

