﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CustomFramework.ServiceLocator.Sample
{
    public class SampleSceneOnePanel : MonoBehaviour
    {
        private SampleSceneHandlerSystem _scenehandlerSystem;
        private SampleScoreHandlerSystem _scoreHandlerSystem;

        private Text _text;
        
        private void Awake()
        {
            _scenehandlerSystem = ServiceLocatorSystem
                .GetOrCreateSystem<SampleSceneHandlerSystem>()
                .FromProjectLocator();

            _scoreHandlerSystem = ServiceLocatorSystem
                .GetOrCreateSystem<SampleScoreHandlerSystem>()
                .FromProjectLocator();

            _text = ServiceLocatorSystem
                .ResolveDependecy<Text>()
                .TakeOne("score_text");
        }

        private void Start() => UpdateScoreText();
        public void OnCLickChangeScene() => _scenehandlerSystem.LoadSecondScene();
        public void OnClickScoreUp()
        {
            _scoreHandlerSystem.ScoreUp();
            UpdateScoreText();
        }

        private void UpdateScoreText() => _text.text = $"click to score up ({_scoreHandlerSystem.Score})";
    }

}
