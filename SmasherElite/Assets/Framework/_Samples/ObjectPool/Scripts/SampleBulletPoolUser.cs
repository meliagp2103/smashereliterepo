﻿using CustomFramework.ServiceLocator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleBulletPoolUser : MonoBehaviour
    {
        private SampleBulletPool _bulletPool;

        private void Awake()
        {
            _bulletPool = ServiceLocatorSystem
                .ResolveDependecy<SampleBulletPool>()
                .TakeOne("bullet_pool");
        }
        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Space))
                _bulletPool.Spawn(transform);
        }
    
    }
}

