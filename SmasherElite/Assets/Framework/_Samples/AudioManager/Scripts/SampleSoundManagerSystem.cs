﻿using CustomFramework.ServiceLocator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleSoundManagerSystem : AAudioManagerSystem
    {
        protected override void Init()
        {
            audioSource = ServiceLocatorSystem
                .ResolveDependecy<AudioSource>()
                .TakeOne("global_source");

            audioSettings = ServiceLocatorSystem
                .ResolveDependecy<AudioClipContainer>()
                .TakeOne("scene_audio_clips");
        }
    }
}

