﻿using CustomFramework.ServiceLocator;
using TMPro;
using UnityEngine;
using UnityEngine.Advertisements;

namespace CustomFramework.Managers.Sample
{
    public class SampleAdvertiserSystem :  AAdvertiseSystem
    {
        private TextMeshProUGUI _console;
        public SampleAdvertiserSystem()
        {
            _settings = ServiceLocatorSystem
                .ResolveDependecy<AdvertiseSystemSetting>()
                .TakeOne();

            Initialize();
            
            _console = ServiceLocatorSystem
                .ResolveDependecy<TextMeshProUGUI>()
                .TakeOne("console");
        }

        public override void OnUnityAdsReady(string placementId)
        {
            Debug.Log($"Ads Ready {placementId}");
            _console.text = $"Ads Ready {placementId}";
        }
        
        public override void OnUnityAdsDidError(string message)
        {
            Debug.Log($"Ads Error {message}");
            _console.text = $"Ads Error {message}";
        }
        
        public override void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            Debug.Log($"ads finish {placementId} {showResult}");
            _console.text = $"ads finish {placementId} {showResult}";
        }
        
        public override void OnUnityAdsDidStart(string placementId)
        {
            Debug.Log($"ads start {placementId}");
            _console.text = $"ads start {placementId}";
        }
    }    
}

