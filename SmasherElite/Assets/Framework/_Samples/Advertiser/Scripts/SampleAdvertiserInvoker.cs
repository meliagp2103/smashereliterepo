﻿using CustomFramework.ServiceLocator;
using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleAdvertiserInvoker : MonoBehaviour
    {
        public AdsType AdsToInvoke;
        private SampleAdvertiserSystem _advertiseSystem;
        private void Awake()
        {
            _advertiseSystem = ServiceLocatorSystem
                .GetOrCreateSystem<SampleAdvertiserSystem>()
                .FromSceneLocator();
        }

        public void InvokeAds()
        {
            _advertiseSystem.ShowAd(AdsToInvoke);
        }
    }
}

