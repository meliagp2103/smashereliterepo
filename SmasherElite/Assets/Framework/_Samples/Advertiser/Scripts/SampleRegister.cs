﻿using CustomFramework.ServiceLocator;
using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleRegister : ARegister
    {
        [SerializeField] private AdvertiseSystemSetting _advertiseSystemSetting;
        public override void Register()
        {
            ServiceLocatorSystem
                .Register(_advertiseSystemSetting)
                .WithID("")
                .AsSceneAccess();
        }
    }
}