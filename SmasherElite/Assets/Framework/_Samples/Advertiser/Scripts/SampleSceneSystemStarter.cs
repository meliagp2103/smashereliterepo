﻿using System;
using CustomFramework.ServiceLocator;
using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleSceneSystemStarter : MonoBehaviour
    {
        private void Awake()
        {
            ServiceLocatorSystem
                .GetOrCreateSystem<SampleAdvertiserSystem>()
                .FromSceneLocator()
                .ShowBanner();
        }
    }
}