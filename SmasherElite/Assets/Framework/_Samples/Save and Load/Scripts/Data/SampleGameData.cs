﻿using CustomFramework.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleGameData
    {
        public int Score;
        public static readonly string saveName = "sample";
    }

}
