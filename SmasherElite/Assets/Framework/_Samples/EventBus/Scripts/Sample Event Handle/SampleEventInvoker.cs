﻿using CustomFramework.ServiceLocator;
using System;
using UnityEngine;

namespace CustomFramework.EventBus.Sample
{
    public class SampleEventInvoker : MonoBehaviour
    {
        private EventBusSystem _eventBus;
#pragma warning disable 649
        [SerializeField] private Button_ID _button_id;
#pragma warning restore 649
        private void Awake()
        {
            _eventBus = ServiceLocatorSystem.GetOrCreateSystem<EventBusSystem>().FromSceneLocator();
        }
        public void InvokeEvent()
        {
            _eventBus.InvokeEvent(new PressedButtonEvent()
            {
                Button_ID = _button_id,
                PressTime = DateTime.Now,
            });
        }
    }
}

