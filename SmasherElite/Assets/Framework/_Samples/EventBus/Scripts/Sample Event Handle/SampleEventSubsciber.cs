﻿using CustomFramework.ServiceLocator;
using UnityEngine;
using UnityEngine.UI;

namespace CustomFramework.EventBus.Sample
{
    [RequireComponent(typeof(Text))]
    public class SampleEventSubsciber : MonoBehaviour
    {
        private EventBusSystem _eventBus;
        private Text _text;

        private void Awake()
        {
            _text = GetComponent<Text>();
            _eventBus = ServiceLocatorSystem.GetOrCreateSystem<EventBusSystem>().FromSceneLocator();
            _eventBus.Subscribe<PressedButtonEvent>(OnButtonPressed);
        }
        private void OnDestroy()
        {
            _eventBus.Unsubscribe<PressedButtonEvent>(OnButtonPressed);
        }
        private void OnButtonPressed(PressedButtonEvent pressedButtonEvent)
        {
            _text.text = $"{pressedButtonEvent.Button_ID} is pressed {pressedButtonEvent.PressTime}";
        }
    }
}

