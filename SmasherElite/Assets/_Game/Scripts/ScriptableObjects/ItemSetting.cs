﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Settings/Item")]
public class ItemSetting : ScriptableObject
{
    public float forceApplied;
}