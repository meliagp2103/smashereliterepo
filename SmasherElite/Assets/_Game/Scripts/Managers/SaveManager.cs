﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomFramework.Managers;
using CustomFramework.ServiceLocator;
using CustomFramework.EventBus;

public class SaveManager : ASaveAndLoadManagerSystem
{
    public SaveManager()
    {
        _savableList = ServiceLocatorSystem
            .ResolveDependecy<ISavable>()
            .GetAll();
    }
}
