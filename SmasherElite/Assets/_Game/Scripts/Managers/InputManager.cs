﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using CustomFramework.Managers;
using CustomFramework.ServiceLocator;
using CustomFramework.EventBus;

public class InputManager : AInputManager
{
    private EventBusSystem eventBusSystem;

    private void Awake()
    {
        eventBusSystem = ServiceLocatorSystem
            .GetOrCreateSystem<EventBusSystem>()
            .FromSceneLocator();
    }

    public void SimpleTap(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            Gesture = Gesture.Tap;
        }

        if (context.canceled)
        {
            Gesture = Gesture.NotProcessed;
        }
    }

    protected override void OnGesture(GestureInfo gestureInfo)
    {
        eventBusSystem.InvokeEvent(gestureInfo);
    }
}
