﻿using UnityEngine;
using CustomFramework.Managers;
using CustomFramework.Utils;
using CustomFramework.ServiceLocator;
using CustomFramework.EventBus;
using System;

public class GameManager : AGameStateManagerSystem, ISavable
{
    private GameData gameData;

    public int Score { get; private set; }

    public int BestScore { get => gameData.bestScore; set => gameData.bestScore = value; }

    private readonly string saveName = "GameManager";

    private EventBusSystem eventBus;

    private ScoresRefreshInfo ScoresRefreshInfos => new ScoresRefreshInfo() { Score = Score, BestScore = BestScore };

    private DateTime timer;

    private TimeSpan counter = new TimeSpan(0, 1, 0);

    public TimeSpan DeltaTime
    {
        get
        {
            var timeRemainimg = timer - DateTime.Now;

            if(timeRemainimg < TimeSpan.Zero)
            {
                SetEndGameState();
            }

            return timeRemainimg;
        }
    }

    public float Minutes => DeltaTime.Minutes;

    public float Seconds => DeltaTime.Seconds;

    public GameManager()
    {
        eventBus = ServiceLocatorSystem
            .GetOrCreateSystem<EventBusSystem>()
            .FromSceneLocator();
    }

    public void Load()
    {
        gameData = FileStreamUtility.ReadBinary<GameData>(saveName);
    }

    public void ResetData()
    {
        FileStreamUtility.ResetFile<GameData>(saveName);
    }

    public void Save()
    {
        FileStreamUtility.WriteBinary(gameData, saveName);
    }

    private void ScoreEventInvoke() => eventBus.InvokeEvent(new ScoreInfo() { Score = Score, BestScore = BestScore});

    public void RoundStart()
    {
        Score = 0;
        timer = DateTime.Now + counter;
        Debug.Log("Timer: " + timer);
        ScoreRefresh();
        SetRunState();
    }

    private void ScoreRefresh()
    {
        eventBus.InvokeEvent<ScoresRefreshInfo>(ScoresRefreshInfos);
    }

    protected override void OnGameStateChange(GameState newState)
    {
        switch (newState)
        {
            case GameState.Running:
                break;
            case GameState.Pause:
                break;
            case GameState.EndGame:
                if (Score > BestScore)
                {
                    BestScore = Score;
                    Save();
                }
                ScoreRefresh();
                break;
        }
        eventBus.InvokeEvent(newState);
    }

   

    public void ScoreUp()
    {
        Score++;

        if(Score > BestScore)
        {
            BestScore = Score;
        }

        ScoreEventInvoke();
    }

}

public struct ScoreInfo
{
    public int Score;
    public int BestScore;
}

public struct ScoresRefreshInfo
{
    public int Score;
    public int BestScore;
}