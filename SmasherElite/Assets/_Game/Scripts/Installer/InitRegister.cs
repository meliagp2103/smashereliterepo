﻿using CustomFramework.ServiceLocator;
using CustomFramework.EventBus;
using CustomFramework.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitRegister : ARegister
{

    private GameManager gameManager;

    public override void Register()
    {
        var eventBusSystem = ServiceLocatorSystem
        .GetOrCreateSystem<EventBusSystem>()
        .FromSceneLocator();

        eventBusSystem.DeclareEvent<GestureInfo>();

        eventBusSystem.DeclareEvent<ScoreInfo>();

        eventBusSystem.DeclareEvent<ScoresRefreshInfo>();

        eventBusSystem.DeclareEvent<GameState>();

        gameManager = ServiceLocatorSystem.
            GetOrCreateSystem<GameManager>()
            .FromSceneLocator();

        ServiceLocatorSystem
            .Register(new InputProcessorSystem())
            .AsSceneAccess();

        ServiceLocatorSystem
            .Register(new GameManager())
            .AsSceneAccess();

        var saveManagerSystem = ServiceLocatorSystem
         .GetOrCreateSystem<SaveManager>()
         .FromSceneLocator();

        saveManagerSystem.LoadAll();
    }

    private void Start()
    {
        gameManager.RoundStart();
    }
}
