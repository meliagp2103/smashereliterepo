﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using CustomFramework.Managers;
using CustomFramework.ServiceLocator;
using CustomFramework.EventBus;
using CustomFramework.Utils;
using System.Threading.Tasks;

public class InputProcessorSystem
{
    private EventBusSystem eventBusSystem;

    private bool isTracking;

    private GameManager gameManager;

    private Transform line;

     List<Vector3> trackPoints = new List<Vector3>();

   public InputProcessorSystem()
   {
        eventBusSystem = ServiceLocatorSystem
            .GetOrCreateSystem<EventBusSystem>()
            .FromSceneLocator();

        eventBusSystem.Subscribe<GestureInfo>(OnGestureRecived);

        gameManager = ServiceLocatorSystem
            .GetOrCreateSystem<GameManager>()
            .FromSceneLocator();

        line = ServiceLocatorSystem
            .ResolveDependecy<Transform>()
            .TakeOne("Line");

   }

    ~InputProcessorSystem()
    {
        eventBusSystem.Unsubscribe<GestureInfo>(OnGestureRecived);
    }

    private void OnGestureRecived(GestureInfo info)
    {
        if (info.Gesture == Gesture.Tap)
        {
            switch (info.GesturePhase)
            {
                case GesturePhase.Starting:
                    isTracking = true;
                    TrackingSystem();
                    break;
                case GesturePhase.End:
                    isTracking = false;
                    break;
            }
        }
    }

    //Need to Refactor (maybe a new System and add An "Update Queue" Method)
    private async void TrackingSystem()
    {
        if (gameManager.GameState == GameState.Running)
        {
            var currentPointerPosition = AInputManager.PointerPosition;

            var currentPointerPositionWorld = Camera.main.ScreenToWorldPoint(currentPointerPosition);

            trackPoints.Add(currentPointerPositionWorld);

            while (isTracking)
            {
                if ((AInputManager.PointerPosition - currentPointerPosition).sqrMagnitude > Mathf.Pow(25, 2))
                {

                    var test = new Vector3(currentPointerPosition.x, currentPointerPosition.y, 10);

                    var startPoint = Camera.main.ScreenToWorldPoint(test);

                    //Debug.Log($"Start point: {startPoint}");


                    var test2 = new Vector3(AInputManager.PointerPosition.x, AInputManager.PointerPosition.y, 10);

                    var targetPosition = Camera.main.ScreenToWorldPoint(test2);

                    //Debug.Log($"Target point: {targetPosition}");


                    var pointDelta = targetPosition - startPoint;

                    //Debug.Log("Point Delta" + pointDelta);

                    trackPoints.Add(targetPosition);

                    Debug.DrawRay(startPoint, pointDelta, Color.red, 1f);


                    var raycastResult = Physics.RaycastAll(startPoint, pointDelta, pointDelta.magnitude);

                    foreach (var item in raycastResult)
                    {
                        Debug.Log(item.collider.name);

                        item.collider.GetComponent<ICuttable>().OnCut();
                    }

                    currentPointerPosition = AInputManager.PointerPosition;

                    line.position = targetPosition;
                }

                await Task.Delay(TimeSpan.FromSeconds(Time.deltaTime));
            }
        }
    }
}
