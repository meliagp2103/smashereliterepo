﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitBehaviour : AItemBehaviour
{
    public override void OnDespawnEffect()
    {
        Debug.Log("Score UP");
    }

    public override void OnCut()
    {
        base.OnCut();
        gameManager.ScoreUp();
    }
}
