﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBehaviour : AItemBehaviour
{
    public override void OnDespawnEffect()
    {
        Debug.Log("Game Over");
    }

    public override void OnCut()
    {
        base.OnCut();
        gameManager.SetEndGameState();
    }
}
