﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomFramework.Managers;
using CustomFramework.ServiceLocator;
using System;


public abstract class AItemBehaviour : MonoBehaviour,IPoolable<Transform,IMemoryPool>,IDisposable,ICuttable
{

    private IMemoryPool pool;

    public ItemSetting item;

    public float forceApllied => item.forceApplied;

    private Rigidbody rb => GetComponent<Rigidbody>();

    protected GameManager gameManager;


    public void Dispose()
    {
        pool.Despawn(this);
    }

    public virtual void OnCut()
    {
        Dispose();
    }

    public void OnCreate()
    {
        gameManager = ServiceLocatorSystem
            .GetOrCreateSystem<GameManager>()
            .FromSceneLocator();
    }

    public void OnDespawn()
    {
        pool = null;
        OnDespawnEffect();
    }

    public abstract void OnDespawnEffect();

    public void OnSpawn(Transform param1, IMemoryPool param2)
    {
        pool = param2;

        transform.position = param1.position;

        transform.rotation = param1.rotation;

        //variable force speed
        rb.AddForce(transform.up * forceApllied, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.name == "DisposeCollider")
        {
            Dispose();
        }
    }
}
