﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomFramework.ServiceLocator;
using System.Linq;

public class ItemSpawner : MonoBehaviour
{
    public List<Transform> spawnPoints;

    public float spawnTime;

    private float lastTimeSpawned;

    private bool canSpawn => Time.time - lastTimeSpawned > spawnTime;

    private GameManager gameManager;

    private List<ItemPool> pools;

    private void Start()
    {
        pools = ServiceLocatorSystem
            .ResolveDependecy<ItemPool>()
            .GetAll();

        gameManager = ServiceLocatorSystem.
            GetOrCreateSystem<GameManager>()
            .FromSceneLocator();
    }

    private void Update()
    {
        if (gameManager.GameState == GameState.Running)
        {
            if (canSpawn)
            {
                float randomPercentage = Random.Range(0, 100f);

                int randomSpawnerIndex = Random.Range(0, spawnPoints.Count);

                var randomSpawner = spawnPoints[randomSpawnerIndex];

                var currentpool = pools[Random.Range(0,pools.Count)];

                var spawnedItem = currentpool.Spawn(randomSpawner);

                lastTimeSpawned = Time.time;
            }
        }
    }
}
