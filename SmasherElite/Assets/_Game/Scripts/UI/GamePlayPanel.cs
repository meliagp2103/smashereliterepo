﻿using CustomFramework.EventBus;
using CustomFramework.ServiceLocator;
using CustomFramework.UI;
using TMPro;
using UnityEngine;

public class GamePlayPanel : BasePanel
{

    public Color defaultColor;

    public Color bestScoreColor;

    private TextMeshProUGUI scoreCounter;

    private TextMeshProUGUI bestScoreCounter;

    private TextMeshProUGUI bestScoreLabel;

    private TextMeshProUGUI countDownText;

    private EventBusSystem eventBus;

    private GameManager gameManager;

    private int tempBestScore;

    private void Start()
    {

        gameManager = ServiceLocatorSystem
            .GetOrCreateSystem<GameManager>()
            .FromSceneLocator();

        scoreCounter = ServiceLocatorSystem
        .ResolveDependecy<TextMeshProUGUI>()
        .TakeOne("score_text");

        bestScoreCounter = ServiceLocatorSystem
            .ResolveDependecy<TextMeshProUGUI>()
            .TakeOne("best_score_text");

        bestScoreLabel = ServiceLocatorSystem
            .ResolveDependecy<TextMeshProUGUI>()
            .TakeOne("best_score_label");

        countDownText = ServiceLocatorSystem
            .ResolveDependecy<TextMeshProUGUI>()
            .TakeOne("countdown_text");

        eventBus = ServiceLocatorSystem
           .GetOrCreateSystem<EventBusSystem>()
           .FromSceneLocator();

        eventBus.Subscribe<ScoreInfo>(OnScoreUp);

        eventBus.Subscribe<ScoresRefreshInfo>(OnScoreRefresh);
    }

    private void Update()
    {
        if (gameManager.GameState == GameState.Running)
        {
            countDownText.text = $"{gameManager.Minutes.ToString("00")}:{gameManager.Seconds.ToString("00")}";
        }
    }

    private void OnScoreUp(ScoreInfo info)
    {
        if(info.BestScore > tempBestScore)
        {
            bestScoreLabel.color = bestScoreColor;

            bestScoreCounter.color = bestScoreColor;
        }
        else
        {
            bestScoreLabel.color = defaultColor;

            bestScoreCounter.color = defaultColor;
        }

        scoreCounter.text = info.Score.ToString();

        bestScoreCounter.text = info.BestScore.ToString();
    }

    private void OnScoreRefresh(ScoresRefreshInfo infos)
    {
        scoreCounter.text = infos.Score.ToString();

        bestScoreCounter.text = infos.BestScore.ToString();

        tempBestScore = infos.BestScore;
    }

    private void OnDestroy()
    {
        eventBus.Unsubscribe<ScoreInfo>(OnScoreUp);

        eventBus.Unsubscribe<ScoresRefreshInfo>(OnScoreRefresh);
    }
}
